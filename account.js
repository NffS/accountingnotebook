let txTypes = require.main.require("../utils").txTypes;
let instance = null;

module.exports = class Account {
    constructor() {
        if (!instance) {
            instance = this;
        }
        this.balance = 0;
        this.history = [];
        return instance;
    }

    getCurrentBalance() {
        return this.balance
    }

    getTransactionsHistory() {
        return this.history
    }

    processTransaction(params) {
        let success = true, info = "";
        if (params.type === txTypes.DEBIT && this.balance < params.amount) {
            success = false;
            info = "Not enough credit";
        }
        if(success) {
            if (params.type.toLowerCase() === txTypes.DEBIT)
                this.balance -= params.amount;
            else
                this.balance += params.amount;
        }
        let txData =
            {
                id: params.id,
                type: params.type,
                amount: params.amount,
                effectiveDate: new Date(),
                success: success,
                info: info
            };

        this.history.push(txData);
        return success;
    }

};


function addTransaction_(self, txId, type, amount, effectiveDate) {

}
