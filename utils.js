exports.errors = Object.freeze({
        "INVALID_PARAMS"                : {"error": {"code": -32601, "message": "Invalid Params"}},
        "NOT_ENOUGH_CREDIT"             : {"error": {"code": -32602, "message": "Not enough credit"}},
    }
);

exports.txTypes = Object.freeze({
   "CREDIT" : "credit",
   "DEBIT"  : "debit"
});