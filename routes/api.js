var express = require('express');
var router = express.Router();
let txTypes = require.main.require("../utils").txTypes;
let Errors = require.main.require("../utils").errors;
let Account = require.main.require("../account");
let account = new Account();

router.get('/accountInfo', function (req, res, next) {
    let uiData = {
        result: {
            currentBalance: account.getCurrentBalance(),
            history: account.getTransactionsHistory()
        }
    };
    res.send(uiData)
});

router.post('/transaction', function (req, res, next) {
    if (!isValidParams(req.body)) {
        res.status(400);
        res.send(Errors.INVALID_PARAMS);
        return;
    }
    if (!account.processTransaction(req.body)) {
        res.status(418); //TODO: 400, 402, 403 or 422 discuss best response code depends on project rules and business logic.
        res.send(Errors.NOT_ENOUGH_CREDIT);
        return;
    }
    res.send("ok");
});

function isValidParams(params) {
    return !(typeof params.type !== "string" || typeof params.amount !== "number" ||
        (params.type.toLowerCase() !== txTypes.DEBIT && params.type.toLowerCase() !== txTypes.CREDIT) ||
        params.amount <= 0);
}


module.exports = router;