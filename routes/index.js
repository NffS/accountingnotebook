var express = require('express');
var router = express.Router();
let Account = require.main.require("../account");
let account = new Account();

router.get('/', function (req, res, next) {
    let uiData = {
        title: 'Express',
        currentBalance: account.getCurrentBalance(),
        transactions: account.getTransactionsHistory()
    };
    res.render('index', uiData);
});


module.exports = router;
