# Accounting notebook (node js) 
Install: *npm -i*  
Start: *node bin\www* or *npm start*  
Ui page: http://localhost:3003

##API
### `POST` /api/transaction  
  
> Add transaction

**Request:**  
```json  
{
   "id": "_UUID_",
   "type": "debit",
   "amount": 300,
   "effectiveDate": "_DATE_"
}
``` 
|params|description|  
|:--:|:--|  
|**id** *(optional)*|uuid string|
|**type**|[`debit`, `credit`]|
|**amount**|transaction amount. only positive number| 
|**effectiveDate** *(optional)*|transaction date. Ignored in current implementation|

**Response:**  
Response code `200` if ok. Response codes `400` or `418` if error.
 
----------  
### `GET` /api/accountInfo  
  
> Get account balance and transaction history

**Response:**  
```json  
{
    "result": {
        "currentBalance": 300,
        "history": [
            {
                "type": "debit",
                "amount": 300,
                "effectiveDate": "2018-08-13T09:06:23.789Z",
                "success": false,
                "info": "Not enough credit"
            },
            ...
        ]
    }
}
``` 
|params|description|  
|:--:|:--|  
|**currentBalance**|Current account balance|
|**history**|array with transaction history|
